package by.training.elevator;

import by.training.elevator.beans.Floor;
import by.training.elevator.enums.ControllerActions;
import by.training.elevator.enums.Direction;
import by.training.elevator.enums.TransportationState;

import by.training.elevator.beans.Building;
import by.training.elevator.tasks.Controller;
import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Passenger;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestController {

    private Controller controller;
    private Building building;
    private Elevator elevator;
    private Passenger passenger;

    /**
     * Prepare.
     */
    @Before
    public void setup() {
        final int passengerId = 0;
        final int passengerStartingFloorNumber = 1;
        final int passengerDestinationFloorNumber = 2;
        building = new Building(10, 6, 100);
        controller = new Controller(building);
        elevator = Controller.getElevator();
        passenger = new Passenger(passengerId, passengerStartingFloorNumber, passengerDestinationFloorNumber);
    }

    /**
     * Check passenger has transportation task.
     */
    @Test
    public void checksIfEachPassengerHasTransportationTask() {
        for (Passenger passenger : building.getPassengers()) {
            assertNotNull("Passenger has transportation task",
                    passenger.getTransportationTask());
        }
    }

    /**
     * Check current floor number change after elevator moved.
     */
    @Test
    public void returnChangedFloorNumberAfterMoving() {
        int FloorNumberBeforeMoving = elevator.getCurrentFloorNumber();
        controller.moveElevator();
        int FloorNumberAfterMoving = elevator.getCurrentFloorNumber();
        assertThat("Floor number changed after elevator moving",
                FloorNumberBeforeMoving, not(FloorNumberAfterMoving));
    }

    /**
     * Check current floor number incremented after elevator moved.
     */
    @Test
    public void returnGreaterNumberOfFloorAfterMovingWhenDirectionOfElevatorIsUp() {
        int FloorNumberBeforeMoving = elevator.getCurrentFloorNumber();
        controller.moveElevator();
        int FloorNumberAfterMoving = elevator.getCurrentFloorNumber();
        assertTrue("Floor number becomes greater after elevator moving",
                FloorNumberAfterMoving > FloorNumberBeforeMoving);
    }

    /**
     * Check current floor number decremented after elevator moved down.
     */
    @Test
    public void returnLowerNumberOfFloorAfterMovingWhenDirectionOfElevatorIsDown() {
        Controller.setMovementDirection(Direction.DOWN);
        int FloorNumberBeforeMoving = elevator.getCurrentFloorNumber();
        controller.moveElevator();
        int FloorNumberAfterMoving = elevator.getCurrentFloorNumber();
        assertTrue("Floor number becomes lower after elevator moving",
                FloorNumberAfterMoving < FloorNumberBeforeMoving);
    }

    /**
     * Check changing direction in DOWN  elevator have maximal floor.
     */
    @Test
    public void changeMovementDirectionWhenReachesExtremeFloor() {
        elevator.setCurrentFloorNumber(building.getFloorsNumber());
        Direction movementDirectionBeforeMoving =
                Controller.getMovementDirection();
        controller.moveElevator();
        Direction movementDirectionAfterMoving =
                Controller.getMovementDirection();
        assertThat("Movement direction change after elevator moving",
                movementDirectionBeforeMoving, not(movementDirectionAfterMoving));
    }

    /**
     * Check controller action.
     */
    @Test
    public void returnMovingElevatorAsElevatorControllerAction() {
        controller.moveElevator();
        ControllerActions elevatorControllerAction = Controller.getAction();
        assertThat("Elevator controller action is MOVING_ELEVATOR",
                elevatorControllerAction, is(ControllerActions.MOVING_ELEVATOR));
    }

    /**
     * Check passenger transportation state.
     */
    @Test(timeout = 2000)
    public void checkIfLeavingOnlyElevatorPassengers() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(passenger.getTransportationTask());
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Controller.setCountDownLatch(countDownLatch);
        controller.waitPassengers();
        controller.informPassengersLeaveElevator();
        TransportationState passengerTransportationState = passenger.getTransportationState();
        executorService.shutdown();
        assertThat("Passenger who was not in elevator transportation state is not COMPLETED",
                passengerTransportationState, not(TransportationState.COMPLETED));
    }

    /**
     * Check elevator capacity with passengers.
     */
    @Test
    public void checkIfInElevatorCanNotBeMorePassengersThanItsCapacity() {
        final int addToOverflowElevatorCapacity = 1;
        int elevatorCapacity = building.getElevatorCapacity();
        int overflowElevatorCapacity = elevatorCapacity + addToOverflowElevatorCapacity;
        for (int i = 0; i < overflowElevatorCapacity; i++) {
            Passenger passenger = mock(Passenger.class);
            when(passenger.getId()).thenReturn(i);
            elevator.addPassenger(passenger);
        }
        int passengersNumber = elevator.getPassengers().size();
        assertThat("Passengers number equal elevator capacity",
                passengersNumber, is(building.getElevatorCapacity()));
    }

    /**
     * Check controller action "all passengers is arrived".
     */
    @Test
    public void returnCompletionTransportationAsElevatorControllerAction() {
        for (Floor Floor : building.getFloors()) {
            freeFloorByDispatchPassengers(Floor);
        }
        controller.isTransportationFinished();
        ControllerActions elevatorControllerAction = Controller.getAction();
        assertThat("Elevator controller action is COMPLETION_TRANSPORTATION",
                elevatorControllerAction, is(ControllerActions.COMPLETION_TRANSPORTATION));
    }

    /**
     * Check elevator is empty if "all passengers is arrived".
     */
    @Test
    public void checkIfElevatorIsEmpty() {
        for (Floor Floor : building.getFloors()) {
            freeFloorByDispatchPassengers(Floor);
        }
        controller.isTransportationFinished();
        ControllerActions elevatorControllerAction = Controller.getAction();
        assertThat("Elevator controller action is COMPLETION_TRANSPORTATION",
                elevatorControllerAction, is(ControllerActions.COMPLETION_TRANSPORTATION));
        boolean isElevatorEmpty = elevator.isEmpty();
        assertTrue("Elevator is empty", isElevatorEmpty);
    }

    /**
     * Check all dispatch container is empty if "all passengers is arrived".
     */
    @Test
    public void checkIfNoPassengersWaitingForTransportation() {
        final int zeroPassengersNumber = 0;
        for (Floor Floor : building.getFloors()) {
            freeFloorByDispatchPassengers(Floor);
        }
        Controller.arrivedPassengers.set(building.getPassengersNumber());
        controller.isTransportationFinished();
        ControllerActions elevatorControllerAction = Controller.getAction();
        assertThat("Elevator controller action is COMPLETION_TRANSPORTATION",
                elevatorControllerAction, is(ControllerActions.COMPLETION_TRANSPORTATION));
        for (Floor Floor : building.getFloors()) {
            int FloorDispatchPassengersNumber = Floor.getDispatchPassengersNumber();
            assertThat("No passengers waiting for transportation",
                    FloorDispatchPassengersNumber, is(zeroPassengersNumber));
        }
    }

    /**
     * Passenger enter in elevator.
     * @param passenger .
     */
    private void EnteringPassenger(final Passenger passenger) {
        final int passengersNumber = 1;
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(passenger.getTransportationTask());
        if (Controller.getMovementDirection() == passenger.getDirection()) {
            CountDownLatch countDownLatch = new CountDownLatch(passengersNumber);
            Controller.setCountDownLatch(countDownLatch);
            controller.waitPassengers();
        }
        controller.informPassengersEnterElevator();
        executorService.shutdown();
    }

    /**
     * Clear all passenger from dispatch container on floor.
     * @param floor .
     */
    private void freeFloorByDispatchPassengers(final Floor floor) {
        Collection<Passenger> dispatchPassengers = floor.getDispathPassengers();
        for (Passenger dispatchPassenger : dispatchPassengers) {
            int dispatchPassengerId = dispatchPassenger.getId();
            floor.removeDispatchPassenger(dispatchPassengerId);
        }
    }
}
