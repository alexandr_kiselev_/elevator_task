package by.training.elevator.enums;

/***
 * Passenger's transportation state.
 */
public enum TransportationState {
    /**
     * Create passenger object.
     */
    NOT_STARTED,
    /**
     * Transportation in progress.
     */
    IN_PROGRESS,
    /**
     * Transportation completed.
     */
    COMPLETED
}
