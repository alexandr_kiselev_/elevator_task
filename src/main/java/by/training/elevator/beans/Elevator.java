package by.training.elevator.beans;

import java.util.Collection;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class elevator contain information about passengers in elevator,
 * methods add and remove passengers, move elevator from floor to floor.
 */
public class Elevator {
    /**
     * Current floor number.
     */
    private int currentFloorNumber;
    /**
     * Elevator capacity.
     */
    private int elevatorCapacity;
    /**
     * Elevator container.
     */
    private Container elevatorContainer;
    /**
     * Lock for elevator.
     */
    private Lock lock;
    /**
     * Condition for elevator.
     */
    private Condition condition;

    /***
     * @param elevatorCapacity .
     * @param startFloorNumber .
     */
    Elevator(final int elevatorCapacity, final int startFloorNumber) {
        super();
        this.elevatorCapacity = elevatorCapacity;
        this.currentFloorNumber = startFloorNumber;
        this.elevatorContainer = new Container();
        this.lock = new ReentrantLock();
        this.condition = lock.newCondition();
    }

    /**
     * @return elevator's lock.
     */
    public Lock getLock() {
        return lock;
    }

    /**
     * @return elevator's condition.
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * @return current floor number.
     */
    public int getCurrentFloorNumber() {
        return currentFloorNumber;
    }

    /**
     * @param currentFloorNumber .
     */
    public void setCurrentFloorNumber(final int currentFloorNumber) {
        this.currentFloorNumber = currentFloorNumber;
    }

    /**
     * Move up elevator on one floor.
     */
    public void moveUp() {
        currentFloorNumber++;
    }

    /**
     * Move down elevator on one floor.
     */
    public void moveDown() {
        currentFloorNumber--;
    }

    /**
     * Add passenger to elevator.
     *
     * @param passenger .
     */
    public void addPassenger(final Passenger passenger) {
        if (getCurrentCapacity() > 0) {
            elevatorContainer.addPassenger(passenger);
        }
    }

    /***
     * Remove passenger to elevator.
     * @param passengerId .
     */
    public void removePassenger(final int passengerId) {
        elevatorContainer.removePassenger(passengerId);
    }

    /**
     * @return free space in elevator.
     */
    public int getCurrentCapacity() {
        return elevatorCapacity - elevatorContainer.getPassengersNumber();
    }

    /***
     * @return collection of passengers in elevator.
     */
    public Collection<Passenger> getPassengers() {
        return elevatorContainer.getPassengers();
    }

    /**
     * @return true if elevator is empty.
     */
    public boolean isEmpty() {
        if (elevatorContainer.getPassengersNumber() > 0) {
            return false;
        }
        return true;
    }

    /**
     * @param passengerId .
     * @return true if elevator has passengers with passengerId.
     */
    public boolean hasPassenger(final int passengerId) {
        return elevatorContainer.hasPassenger(passengerId);
    }

}
