package by.training.elevator.beans;

import by.training.elevator.enums.Direction;
import by.training.elevator.enums.TransportationState;
import by.training.elevator.tasks.TransportationTask;

/**
 * Class passenger contain information about passenger's transportation and status.
 */
public class Passenger {
    /**
     * Start floor.
     */
    private int startFloor;
    /**
     * Destination floor.
     */
    private int destinationFloor;
    /**
     * Passenger's id contain count of passenger when object is creating.
     */
    private int id;
    /**
     * direction.
     */
    private Direction direction;
    /**
     * transportation state.
     */
    private TransportationState transportationState;

    private TransportationTask transportationTask;

    public Passenger(final int id, final int startFloor, final int destinationFloor) {
        this.startFloor = startFloor;
        this.destinationFloor = destinationFloor;
        this.id = id;

        if (destinationFloor > startFloor)
            this.direction = Direction.UP;
        else
            this.direction = Direction.DOWN;

        transportationState = TransportationState.NOT_STARTED;
        transportationTask = new TransportationTask(this);
    }

    public Direction getDirection() {
        return direction;
    }

    public int getStartFloor() {
        return startFloor;
    }

    public int getDestinationFloor() {
        return destinationFloor;
    }

      public int getId() {
        return id;
    }

    public TransportationState getTransportationState() {
        return transportationState;
    }

    public void setTransportationState(TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    public TransportationTask getTransportationTask() {
        return transportationTask;
    }
}
