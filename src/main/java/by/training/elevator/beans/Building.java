package by.training.elevator.beans;

import by.training.elevator.constants.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class releases methods for work with building.
 */
public class Building {
    /**
     * Floors number.
     */
    final private int floorsNumber;
    /**
     * Elevator capacity.
     */
    final private int elevatorCapacity;
    /**
     * Passengers number.
     */
    final private int passengersNumber;
    /**
     * Elevator.
     */
    private Elevator elevator;
    /**
     * Passengers list.
     */
    private List<Passenger> passengersList;
    /**
     * Map contain number of floor as key and floor object as value.
     */
    private Map<Integer, Floor> floorsMap;

    /**
     * @return passengers number.
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }

    /**
     * @return floors number.
     */
    public int getFloorsNumber() {
        return floorsNumber;
    }

    /**
     * @return elevator capacity.
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * @return elevator.
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * @param floor number.
     * @return floor.
     */
    public Floor getFloor(final int floor) {
        return floorsMap.get(floor);
    }

    /***
     * @param floorsNumber .
     * @param elevatorCapacity .
     * @param passengersNumber .
     */
    public Building(final int floorsNumber,
                    final int elevatorCapacity,
                    final int passengersNumber) {
        super();
        this.floorsNumber = floorsNumber;
        this.elevatorCapacity = elevatorCapacity;
        this.passengersNumber = passengersNumber;
        this.floorsMap = new HashMap<>();
        this.elevator = new Elevator(elevatorCapacity, Constants.START_FLOOR_FOR_ELEVATOR);
        this.passengersList = new ArrayList<>();
        createFloors();
        createPassengers();
        putPassengersOnFloors();
    }

    /**
     * @return passengers list.
     */
    public List<Passenger> getPassengers() {
        return passengersList;
    }

    /**
     * @return floors.
     */
    public Collection<Floor> getFloors() {
        return floorsMap.values();
    }

    /**
     * Create floors.
     */
    private void createFloors() {
        for (int i = 1; i <= floorsNumber; i++) {
            Floor floor = new Floor(i);
            floorsMap.put(i, floor);
        }
    }

    /**
     * @return random floor's number.
     */
    private int getRandomFloor() {
        Random random = new Random();
        return random.nextInt(floorsNumber) + 1;
    }

    /**
     * Create passengers.
     */
    private void createPassengers() {
        for (int i = 1; i <= passengersNumber; i++) {
            int startFloor = getRandomFloor(); // (i - 1) / 10 + 1; //
            int destinationFloor = getRandomFloor();
            while (startFloor == destinationFloor) {
                destinationFloor = getRandomFloor();
            }
            Passenger passenger = new Passenger(i,
                    startFloor,
                    destinationFloor);
            passengersList.add(passenger);
        }
    }

    /**
     * Put passengers on dispatch container on floors.
     */
    private void putPassengersOnFloors() {
        for (Passenger passenger : passengersList) {
            int passengerStartFloor = passenger.getStartFloor();
            floorsMap.get(passengerStartFloor).addDispatchPassenger(passenger);
        }
    }

    /**
     * @param elevator .
     */
    public void setElevator(final Elevator elevator) {
        this.elevator = elevator;
    }

    /**
     * @param floors .
     */
    public void setFloors(final Map<Integer, Floor> floors) {
        this.floorsMap = floors;
    }
}
