package by.training.elevator.beans;

import java.util.Collection;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class floor contain number of floor,
 * dispatch and arrival containers for passengers,
 * method for get/set/remove passengers from/to containers.
 */
public class Floor {
    /**
     * Floor number.
     */
    private int floorNumber;
    /**
     * Dispatch container.
     */
    private Container dispatchFloorContainer;
    /**
     * Arrival container.
     */
    private Container arrivalFloorContainer;
    /**
     * Lock for floor.
     */
    private Lock lock;
    /**
     * Condition for floor.
     */
    private Condition condition;

    /**
     * @param floorNumber .
     */
    Floor(final int floorNumber) {

        this.floorNumber = floorNumber;
        this.dispatchFloorContainer = new Container();
        this.arrivalFloorContainer = new Container();
        this.lock = new ReentrantLock();
        this.condition = lock.newCondition();
    }

    /**
     * @return floor's lock.
     */
    public Lock getLock() {
        return lock;
    }

    /**
     * @return floor's condition.
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * @return floor's number.
     */
    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * @return count passengers in arrival container.
     */
    public int getArrivalPassengersNumber() {
        return arrivalFloorContainer.getPassengersNumber();
    }

    /**
     * @param passenger .
     */
    public void addDispatchPassenger(final Passenger passenger) {
        this.dispatchFloorContainer.addPassenger(passenger);
    }

    /**
     * @param passenger .
     */
    public void addArrivalPassenger(final Passenger passenger) {
        this.arrivalFloorContainer.addPassenger(passenger);
    }

    /**
     * @return count passengers in dispatch container.
     */
    public int getDispatchPassengersNumber() {
        return dispatchFloorContainer.getPassengersNumber();
    }

    /**
     * @param passengerId .
     */
    public void removeDispatchPassenger(final int passengerId) {
        this.dispatchFloorContainer.removePassenger(passengerId);
    }

    /**
     * @return collection of passengers in dispatch container.
     */
    public Collection<Passenger> getDispathPassengers() {
        return this.dispatchFloorContainer.getPassengers();
    }

    /**
     * @return collection of passengers in arrival container.
     */
    public Collection<Passenger> getArrivalPassengers() {
        return this.arrivalFloorContainer.getPassengers();
    }

}
