package by.training.elevator.tasks;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;

import by.training.elevator.beans.Building;
import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Floor;
import by.training.elevator.beans.Passenger;
import by.training.elevator.constants.Constants;
import by.training.elevator.enums.ControllerActions;
import by.training.elevator.enums.Direction;
import by.training.elevator.utils.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class releases methods to work with passengers and check
 * for enter/leave elevator, wakeup passenger's threads.
 */
public class Controller implements Runnable {
    /**
     * Logger for this class.
     */
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    /**
     * .
     */
    private static Building building;
    /**
     * .
     */
    private static ControllerActions action;
    /**
     * .
     */
    private static Direction movementDirection;
    /**
     * .
     */
    public static CountDownLatch countDownLatch;
    /**
     * Count arrived passengers.
     */
    public static AtomicInteger arrivedPassengers = new AtomicInteger(0);

    /**
     * Setup controller.
     *
     * @param building .
     */
    public Controller(final Building building) {
        Controller.building = building;
        movementDirection = Direction.UP;
        countDownLatch = new CountDownLatch(building.getPassengersNumber());
    }

    /***
     * @return current floor number.
     */
    public static int getCurrentFloorNumber() {
        Elevator elevator = building.getElevator();
        return elevator.getCurrentFloorNumber();
    }

    /**
     * @return elevator.
     */
    public static Elevator getElevator() {
        Elevator elevator = building.getElevator();
        return elevator;
    }

    /**
     * @return controller's action.
     */
    public static ControllerActions getAction() {
        return action;
    }

    /**
     * @return direction.
     */
    public static Direction getMovementDirection() {
        return movementDirection;
    }

    /**
     * @param movementDirection .
     */
    public static void setMovementDirection(final Direction movementDirection) {
        Controller.movementDirection = movementDirection;
    }

    /**
     * countDownLatch decrement on 1.
     */
    public static void reduceCountDownLatch() {
        Controller.countDownLatch.countDown();
    }

    /**
     * @param countDownLatch .
     */
    public static void setCountDownLatch(final CountDownLatch countDownLatch) {
        Controller.countDownLatch = countDownLatch;
    }

    /**
     * @param number floor.
     * @return floor.
     */
    public static Floor getFloor(final int number) {
        return building.getFloor(number);
    }

    /***
     * When thread is in progress wakeup passenger's process to leave/enter
     * elevator, moving elevator, and check task to complete, if  not complete
     * repeat all process. When task complete - validate task and end program.
     */
    @Override
    public void run() {
        waitPassengers();
        action = ControllerActions.STARTING_TRANSPORTATION;
        LOGGER.info(action);
        while (!isTransportationFinished()) {
            informPassengersLeaveElevator();
            informPassengersEnterElevator();
            if (isTransportationFinished()) {
                break;
            }
            moveElevator();
        }
    }

    /**
     * Move elevator and after get right direction for next move.
     */
    public void moveElevator() {
        action = ControllerActions.MOVING_ELEVATOR;
        LOGGER.info(action + Constants.FROM_FLOOR_STRING
                + getCurrentFloorNumber() + Constants.TO_FLOOR_STRING
                + getNextFloorNumber() + Constants.BRACKET_CLOSE_STRING);
        Elevator elevator = building.getElevator();
        switch (movementDirection) {
            case UP:
                elevator.moveUp();
                break;
            case DOWN:
                elevator.moveDown();
                break;
            default:
                throw new EnumConstantNotPresentException(Direction.class,
                        movementDirection.name());
        }
        if (getCurrentFloorNumber() >= building.getFloorsNumber()) {
            movementDirection = Direction.DOWN;
        }
        if (getCurrentFloorNumber() <= 1) {
            movementDirection = Direction.UP;
        }
    }

    /**
     * Wait passengers for enter/leave elevator.
     */
    public void waitPassengers() {
        try {
            countDownLatch.await(Constants.TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("Wait passengers was interrupted. " + e);
        }
    }

    /**
     * Controller check and inform passengers to leaving elevator.
     */
    public void informPassengersLeaveElevator() {
        int leavingPassengersCount = getLeavingPassengersCount();
        if (leavingPassengersCount > 0) {
            action = ControllerActions.DEBOARDING_OF_PASSENGER;
            countDownLatch = new CountDownLatch(leavingPassengersCount);
            Elevator elevator = building.getElevator();
            Lock lock = elevator.getLock();
            lock.lock();
            try {
                elevator.getCondition().signalAll();
            } finally {
                lock.unlock();
            }
            waitPassengers();
        }
    }

    /**
     * Controller check and inform passengers to entering elevator.
     */
    public void informPassengersEnterElevator() {
        int enteringPassengersCount = getEnteringPassengersCount();
        Elevator elevator = building.getElevator();
        int elevatorCurrentCapacity = elevator.getCurrentCapacity();
        if (enteringPassengersCount > 0 && elevatorCurrentCapacity > 0) {
            action = ControllerActions.BOARDING_OF_PASSENGER;
            if (elevatorCurrentCapacity > enteringPassengersCount) {
                countDownLatch = new CountDownLatch(enteringPassengersCount);
            } else {
                countDownLatch = new CountDownLatch(elevatorCurrentCapacity);
            }
            int currentFloorNumber = getCurrentFloorNumber();
            Floor currentFloor = building.getFloor(currentFloorNumber);
            Lock lock = currentFloor.getLock();
            lock.lock();
            try {
                currentFloor.getCondition().signalAll();
            } finally {
                lock.unlock();
            }
            waitPassengers();
        }
    }

    /**
     * Get count passengers to leave elevator on current floor.
     *
     * @return count passengers to leave elevator.
     */
    public int getLeavingPassengersCount() {
        int leavingPassengersCount = 0;
        int currentFloorNumber = getCurrentFloorNumber();
        Elevator elevator = building.getElevator();
        for (Passenger passenger : elevator.getPassengers()) {
            int destinationFloor = passenger.getDestinationFloor();
            if (destinationFloor == currentFloorNumber) {
                leavingPassengersCount++;
            }
        }
        return leavingPassengersCount;
    }

    /**
     * Get count passengers to enter elevator on current floor.
     *
     * @return count passengers to enter elevator.
     */
    private int getEnteringPassengersCount() {
        int currentFloorNumber = getCurrentFloorNumber();
        Floor currentFloor = building.getFloor(currentFloorNumber);
        int enteringPassengersCount = 0;
        for (Passenger passenger : currentFloor.getDispathPassengers()) {
            Direction passengerMovementDirection = passenger.getDirection();
            if (passengerMovementDirection == movementDirection) {
                enteringPassengersCount++;
            }
        }
        return enteringPassengersCount;
    }

    /**
     * Remove passenger from elevator by passengerId.
     *
     * @param passengerId .
     */
    public static void removePassengerFromElevator(final int passengerId) {
        Elevator elevator = building.getElevator();
        elevator.removePassenger(passengerId);
    }

    /**
     * Remove passenger from floor by passengerId.
     *
     * @param passengerId .
     */
    public static void removePassengerFromFloor(final int passengerId, final int floorNumber) {
        Floor currentFloor = building.getFloor(floorNumber);
        currentFloor.removeDispatchPassenger(passengerId);
    }

    /**
     * Add passenger to elevator.
     *
     * @param passenger .
     */
    public static void addPassengerToElevator(final Passenger passenger) {
        Elevator elevator = building.getElevator();
        if (elevator.getCurrentCapacity() > 0) {
            elevator.addPassenger(passenger);
        }
    }

    /***
     * Add passenger to destination floor.
     * @param passenger .
     */
    public static void addArrivalPassenger(final Passenger passenger, int floorNumber) {
        Floor currentFloor = building.getFloor(floorNumber);
        currentFloor.addArrivalPassenger(passenger);
    }

    /**
     * Get next floor number with right direction.
     *
     * @return next floor number.
     */
    public int getNextFloorNumber() {
        int nextFloorNumber = getCurrentFloorNumber();
        switch (movementDirection) {
            case UP:
                nextFloorNumber++;
                break;
            case DOWN:
                nextFloorNumber--;
                break;
            default:
                throw new EnumConstantNotPresentException(Direction.class,
                        movementDirection.name());
        }
        return nextFloorNumber;
    }

    /**
     * Check to end task and validate task.
     *
     * @return true if all passengers in destination floor and elevator is empty.
     */
    public boolean isTransportationFinished() {
        Elevator elevator = building.getElevator();
        if (!elevator.isEmpty()) {
            return false;
        }
        if (building.getPassengersNumber() != Controller.arrivedPassengers.get()) {
            return false;
        }
        for (Floor floor : building.getFloors()) {
            int floorDispatchPassengersNumber = floor.getDispatchPassengersNumber();
            if (floorDispatchPassengersNumber > 0) {
                return false;
            }
        }
        action = ControllerActions.COMPLETION_TRANSPORTATION;
        LOGGER.info(action);
        Validator.validate(building);
        return true;
    }
}
